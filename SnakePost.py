#!/bin/python2.7
# -*- coding: utf-8 -*-

import struct
import logging
import sys

import random as r

log = logging.getLogger('SnakePost')


class SnakePost(object):
    """ Protocol for sending secured message """

    def __init__(self, sub_protocol):
        """ Create a SnakePost object

            :param sub_protocol:    a protocol object with 3 methods defined: onReceive, send, iterate
            :param debug:           if True log debug informations on stderr
        """

        self.ack = 0  # ack to send with messages
        self.wait_ack = False
        self.sec_msg = []
        self.sub_protocol = sub_protocol

    @staticmethod
    def __packetize(data, num_seq, num_ack):
        """ Creates datagram for the SnakePost protocol"""

        return struct.pack('!HH', num_seq, num_ack) + data

    @staticmethod
    def __unPacketize(raw_data):
        """ Extract datagram of the SnakePost protocol"""

        return struct.unpack('!HH', raw_data[:4]) + (raw_data[4:],)

    def connect(self, dest_address, **kwargs):
        self.sub_protocol.connect(dest_address, **kwargs)

    def listen(self, addr=1234):
        return self.sub_protocol.listen(addr)

    def send(self, data, addr=None, secured=False):
        """ Send data through the SnakePost protocol """

        if secured:
            num_seq = r.randint(1, 2**16-1)  # @todo change (because chance of having the same number twice: ~1/2**16 )

            if len(self.sec_msg) == 64:
                raise OverflowError("Secured messages queue is full")

            self.sec_msg.insert(0, (num_seq, data, addr))  # insert data in the begining of the list in order to use it as a FIFO
            self.wait_ack = True

            # The msg to send is the one waiting in the FIFO
            num_seq = self.sec_msg[-1][0]   # Num seq of the secured msg
            data = self.sec_msg[-1][1]      # Data of the secured msg

            log.debug(" -> S{}|a{}".format(num_seq, self.ack))
        else:
            num_seq = 0
            log.debug(" -> n{}|a{}".format(num_seq, self.ack))

        # Self.ack is the acknowledge of the last secured message received (piggy backing)
        raw_bin = SnakePost.__packetize(data, num_seq, self.ack)

        return self.sub_protocol.send(raw_bin, addr)

    def checkSecure(self, callback):
        """ Decorator for a onReceive function in order to receive a secured message """

        #defini une fonction qui va traiter la couche SnakePost
        def decorate(data, src):
            num_seq, num_ack, msg = SnakePost.__unPacketize(data)

            if num_seq != 0:
                log.debug(" <- S{}|a{}".format(num_seq, num_ack))
            else:
                log.debug(" <- n{}|a{}".format(num_seq, num_ack))

            # si on attend un ack d'un message securise
            if self.wait_ack:
                # verif si seq envoyee = nack
                if num_ack == self.sec_msg[-1][0]:
                    # plus besoin de renvoyer le message
                    self.sec_msg.pop()

                    if len(self.sec_msg) == 0:
                        self.wait_ack = False

            # Check if not received again the same secured message
            # Bypass the check if received a non-secured message
            if self.ack != num_seq or num_seq == 0:

                # si num_seq != 0 : on a recu un message securise
                if num_seq != 0:
                    # dire qu'un ack est a envoyer
                    self.ack = num_seq

                    # Imediate ack
                    secured_data = SnakePost.__packetize("", 0, self.ack)
                    self.sub_protocol.send(secured_data, src)

                if msg != "":
                    callback(msg, src)

        return decorate

    def onReceive(self, callback):
        """ Define the function to be called when a message is received """

        return self.sub_protocol.onReceive(self.checkSecure(callback))

    def onConnect(self, callback):
        """ Define the function to be called when an incomming connection occures """

        return self.sub_protocol.onConnect(callback)

    def onAccept(self, callback):
        """ Define the function to be called when a connection is established """

        return self.sub_protocol.onAccept(callback)

    def iterate(self):
        """ This function is intended to be called in an infinite loop """

        self.sub_protocol.iterate()

        # si pas nack
        if len(self.sec_msg) != 0:
            # revois msg
            num_seq = self.sec_msg[-1][0]   # Num seq of the secured msg
            data = self.sec_msg[-1][1]      # Data of the secured msg

            # Self.ack is the acknowledge of the last secured message received (piggy backing)
            secured_data = SnakePost.__packetize(data, num_seq, self.ack)
            self.sub_protocol.send(secured_data, self.sec_msg[-1][2])
            log.debug(" -> re-send S{}|a{}".format(num_seq, self.ack))


#### When launched as a script ####
if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr)
    logging.getLogger('SnakeChan').setLevel(logging.DEBUG)
    logging.getLogger('SnakePost').setLevel(logging.DEBUG)

    def unblocking_read():
        """ Read user input in a non-blocking way using select """

        from select import select

        timeout = 0
        sys.stdout.flush()
        read, _, _ = select([sys.stdin], [], [], timeout)
        if read:
            return sys.stdin.readline()
        else:
            return ""

    #When server requested (the argument is the port number)
    if (len(sys.argv) == 3):
        import SnakeChan as sc
        import SnakeUdp as su
        import time
        import json

        if sys.argv[2] == 'u':
            srv = su.SnakeUdp()
        elif sys.argv[2] == 'c':
            srv = sc.SnakeChan()
        else:
            sys.exit(0)

        print("Server STARTed on port : " + sys.argv[1])

        sec_server = SnakePost(srv)

        i = 0

        cli = None

        @sec_server.onReceive
        def respond(data, src):
            print("{}: {}".format(src, data))

            if sys.argv[2] == 'u':
                global cli
                cli = src

            #sec_server.send("{}.Hello Client {}".format(i, time.ctime()), src, True)

        @sec_server.onConnect
        def print_connect_infos(infos, src):
            print("Connection from {}".format(src))
            print("received {}".format(infos))

            global cli
            cli = src

        sec_server.listen(sys.argv[1])

        while True:
            time.sleep(0.2)
            if cli is not None:
                data = unblocking_read()
                if data != "":
                    if data.strip() == "snakes":
                        msg_snakes = {'snakes': [['mickael', [[20, 9], [20, 10], [20, 11], [20, 12], [20, 13]]], ['florent', [[20, 3], [20, 4], [20, 5], [20, 6], [20, 7]]]]}
                        msg = msg_snakes
                        print msg
                        sec_server.send(json.dumps(msg), cli, False)

                    elif data.strip() == "pinf":
                        msg = {'players_info': [['mickael', 'yellow', 0, False], ['florent', 'red', 3, True]]}
                        print msg
                        sec_server.send(json.dumps(msg), cli, True)
                    elif data.strip() == "grow":
                        msg = {'grow': 'florent'}
                        print msg
                        sec_server.send(json.dumps(msg), cli, True)
                    elif data.strip() == "grow2":
                        msg = {'grow': 'jonay'}
                        print msg
                        sec_server.send(json.dumps(msg), cli, True)

                    elif data.strip() == "gover":
                        msg = {'game_over': 'florent'}
                        print msg
                        sec_server.send(json.dumps(msg), cli, True)

                    elif data.strip() == "food":
                        msg = {'foods': [[6, 24], [17, 32], [25, 8], [13, 17]]}
                        print msg
                        sec_server.send(json.dumps(msg), cli, True)

                    elif data == data.upper():
                        sec_server.send(data.format(time.ctime()), cli, True)
                    else:
                        sec_server.send(data.format(time.ctime()), cli, False)
            sec_server.iterate(200)

#When a client is requested (argument is ip and port to connect to)
    elif len(sys.argv) == 4:
        import SnakeChan as sc
        import SnakeUdp as su
        import time

        print("Client STARTed, connect to ip " + sys.argv[1] + " ,port : " + sys.argv[2])

        if sys.argv[3] == 'u':
            cli = su.SnakeUdp()
        elif sys.argv[3] == 'c':
            cli = sc.SnakeChan()
        else:
            sys.exit(0)

        sec_client = SnakePost(cli)

        if sys.argv[2] == 'u':
            connected = True
        else:
            connected = False

        srv_adr = (sys.argv[1], int(sys.argv[2]))

        @sec_client.onReceive
        def print_data(data, src):
            print("{}: {}".format(src, data))

        @sec_client.onAccept
        def when_connected(src):
            global connected, srv_adr
            connected = True
            srv_adr = src
            print("Connected to {}".format(src))

        sec_client.connect(srv_adr, param1="toto", param2=2, color='blue')
        # sec_client.send("coucou", srv_adr)

        while True:
            time.sleep(0.2)
            if connected is True:
                data = unblocking_read()
                if data != "":
                    if data == data.upper():
                        sec_client.send(data.format(time.ctime()), srv_adr, True)
                    else:
                        sec_client.send(data.format(time.ctime()), srv_adr, False)

            sec_client.iterate(200)
