#!/bin/python2.7
# -*- coding: utf-8 -*-

import socket


class SnakeUdp(object):
    def __init__(self, debug=False):
        self.CONNECTION_TIMEOUT = 0
        self.MAX_DATAGRAM_SIZE = 1024

        self.connect_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.connect_socket.settimeout(self.CONNECTION_TIMEOUT)

        self.port = 0

        # User callback
        self.receptionCallback = None
        self.acceptCallback = None
        self.connectionCallback = None

    def connect(self, dest_address, **kwargs):
        return

    def listen(self, port=1234):
        """ listen on any ip for a given port """

        self.port = port
        self.connect_socket.bind(("0.0.0.0", self.port))

    def onReceive(self, callback):
        """ Define the function to be called when a message is received """

        self.receptionCallback = callback
        return callback

    def onConnect(self, callback):
        """ Define the function to be called when an incomming connection occures """

        self.connectionCallback = callback
        return callback

    def onAccept(self, callback):
        """ Define the function to be called when a connection is established """

        self.acceptCallback = callback
        return callback

    def send(self, data, dest):
        """Send data through the SnakeChan protocol"""

        self.connect_socket.sendto(data, dest)

    def iterate(self):

        datagram = ""
        addr = ()

        try:
            datagram, addr = self.connect_socket.recvfrom(self.MAX_DATAGRAM_SIZE)
        except (socket.timeout, socket.error):
            return

        self.receptionCallback(datagram, addr)
