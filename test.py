#!/bin/python2.7
# -*- coding: utf-8 -*-

import mock
import unittest
import logging
import sys
import socket
import struct

from SnakePost import SnakePost
from SnakeUdp import SnakeUdp


def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

log = logging.getLogger('Unittest')
cli = SnakePost(SnakeUdp())


class SnakePostTestCase(unittest.TestCase):

    @mock.patch('__main__.cli.sub_protocol.connect_socket')
    def test_send(self, mock_socket):
        """ Test a normal transmission """

        print("\n")

        cli.send("test 1", ('127.0.0.1', 1232))

        mock_socket.sendto.assert_called_once_with('{}test 1'.format('\x00\x00\x00\x00'), ('127.0.0.1', 1232))
        mock_socket.reset_mock()
        log.debug(" Normal message correctly send OK")

        cli.send("secure test 1", ('127.0.0.1', 1232), True)

        send_seq, ack = struct.unpack('!HH', mock_socket.sendto.call_args[0][0][0:4])
        send_text = mock_socket.sendto.call_args[0][0][4:]
        assert send_seq != 0
        assert send_text == "secure test 1"
        mock_socket.reset_mock()
        log.debug(" Secured message correctly send OK")

        cli.send("secure test 2", ('127.0.0.1', 1232), True)
        send_seq2, ack = struct.unpack('!HH', mock_socket.sendto.call_args[0][0][0:4])
        send_text = mock_socket.sendto.call_args[0][0][4:]
        assert send_seq2 != 0
        assert send_seq2 == send_seq
        assert send_text == "secure test 1"
        mock_socket.reset_mock()
        log.debug(" Secured message correctly re-send and new secured mesaage queued OK")

        mock_socket.recvfrom.side_effect = socket.error
        cli.iterate()
        send_seq3, ack = struct.unpack('!HH', mock_socket.sendto.call_args[0][0][0:4])
        send_text = mock_socket.sendto.call_args[0][0][4:]
        assert send_seq3 != 0
        assert send_seq3 == send_seq
        assert send_text == "secure test 1"
        mock_socket.reset_mock()
        log.debug(" Secured message correctly re-send OK")

        mock_socket.recvfrom.side_effect = None
        mock_socket.recvfrom.return_value = (struct.pack('!HH', 0, send_seq)+"return msg 1", ('127.0.0.1', 1232))

        @cli.onReceive
        def check_onReceive(data, src):
            assert data == "return msg 1"
            assert src == ('127.0.0.1', 1232)
            log.debug(" Normal message correctly received OK")

        cli.iterate()
        send_seq4, ack = struct.unpack('!HH', mock_socket.sendto.call_args[0][0][0:4])
        send_text = mock_socket.sendto.call_args[0][0][4:]
        assert send_seq4 != 0
        assert send_seq4 != send_seq
        assert send_text == "secure test 2"
        mock_socket.reset_mock()
        log.debug(" First secured message acknowledged")
        log.debug(" Second secured message correctly send OK")

        mock_socket.recvfrom.return_value = (struct.pack('!HH', 123, send_seq)+"return secured msg 1", ('127.0.0.1', 3422))

        @cli.onReceive
        def check_onReceive(data, src):
            assert data == "return secured msg 1"
            assert src == ('127.0.0.1', 3422)
            log.debug(" Secured message correctly received OK")

        cli.iterate()
        send_seq5, ack = struct.unpack('!HH', mock_socket.sendto.call_args[0][0][0:4])
        send_text = mock_socket.sendto.call_args[0][0][4:]
        assert send_seq5 != 0
        assert send_seq5 == send_seq4
        assert send_text == "secure test 2"
        assert ack == 123
        mock_socket.reset_mock()


if __name__ == '__main__':
    logging.basicConfig(stream=sys.stderr)
    logging.getLogger('SnakePost').setLevel(logging.DEBUG)
    logging.getLogger('Unittest').setLevel(logging.DEBUG)

    suite = unittest.TestLoader().loadTestsFromTestCase(SnakePostTestCase)

    print("--- Starting Tests ---")
    unittest.TextTestRunner(verbosity=2).run(suite)
